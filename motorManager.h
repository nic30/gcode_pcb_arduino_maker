#ifndef MOTORMANAGER_H
#define MOTORMANAGER_H

#include <Arduino.h>
#include "config.h"

#ifdef DIRECT_STEPPER
#include "Stepper.h"
#endif

#ifdef I2C_STEPPER
#include "I2C_Stepper.h"
#endif


class MotorManager{
private:
#ifdef I2C_STEPPER
  I2C_Stepper m1 = I2C_Stepper(X_ADDR);
  I2C_Stepper m2 = I2C_Stepper(Y_ADDR);
#endif
#ifdef DIRECT_STEPPER
  Stepper m1 = Stepper(STEPS_PER_TURN, M1_PIN1, M1_PIN2, M1_PIN3, M1_PIN4);
  Stepper m2 = Stepper(STEPS_PER_TURN, M2_PIN1, M2_PIN2, M2_PIN3, M2_PIN4);
#endif
public:
  void feedrate(float);
  void onestep(int,int);
  void release();
  unsigned int step_delay;
  float fr; //feedrate
};


#endif 









