/*
 * GCode arduino 405nm laser CNC machine used for creating pcbs 
 *  - acts just like standard cnc see help()
 *  Nic30 10.2.2014
 */

#include "config.h"
#include "GCodeInterpret.h"

#ifdef I2C_STEPPER
#include <Wire.h>
#endif

char buffer[MAX_BUF];  // where we store the message until we get a ';'
int sofar;  // how much is in the buffer

GCodeInterpret gi = GCodeInterpret(buffer, &sofar);

void setup(){
  Serial.begin(BAUDRATE);
#ifdef I2C_STEPPER
  Wire.begin();
#endif
  gi.init();
}

/**
 * After setup() this machine will repeat loop() forever.
 */
void loop() {
  // listen for serial commands
  while(Serial.available() > 0) {  // if something is available
    char c=Serial.read();  // get it
    Serial.print(c);  // repeat it back so I know you got the message
    if(sofar<MAX_BUF) buffer[sofar++]=c;  // store it
    if(buffer[sofar-1]=='\n' || buffer[sofar-1]==';') break;  // entire message received
  }
  if(sofar>0 && buffer[sofar-1]==';') {
    // we got a message and it ends with a semicolon
    buffer[sofar]=0;  // end the buffer so string functions work right
    Serial.print(F("\r\n"));  // echo a return character for humans
    gi.processCommand();  // do something with the command
    gi.ready();
  }
}







