#ifndef GCODEINTERPRET_H
#define GCODEINTERPRET_H

#include "motorManager.h"

class GCodeInterpret{
private:
  MotorManager mm = MotorManager();
  char mode_abs=1;  // absolute mode?
  float px, py;  // location
  char * buffer;
  int * sofarAddr; //num of bytes in buffer
  void pause(long ms);
  void setLaser(int pwm);
  void position(float npx,float npy);
  void line(float newx,float newy);
  float parsenumber(char code,float val);
  void output(char *code,float val);
  void where();
  void returnHome();
public:
  GCodeInterpret(char * buffer, int * sofarAddr);
  void init();
  void processCommand();
  void help();
  void ready();
};

#endif 

