
#include "I2C_Stepper.h"

void I2C_Stepper::transmitToAddr(char adr, char cmd){
  Wire.beginTransmission(adr);
  Wire.write(cmd);
  Wire.endTransmission();
}


I2C_Stepper::I2C_Stepper(int addr){
  adr = addr;
}

void I2C_Stepper::release(){
  transmitToAddr(adr, CMD_RELEASE);
}

void I2C_Stepper::step(int number_of_steps){
  if(number_of_steps ==-1){
    transmitToAddr(adr, CMD_STEP_BACKWARD);
  }
  if(number_of_steps ==1){
    transmitToAddr(adr, CMD_STEP_FOWARD);
  }
}


