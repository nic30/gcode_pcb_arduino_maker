#ifndef I2C_STEPPER_H
#define I2C_STEPPER_H

#include <Arduino.h>
#include <Wire.h>
#include "config.h"

class I2C_Stepper{
private:
  char adr;
  void transmitToAddr(char adr, char msg);
public:
  I2C_Stepper(int);
  void release();
  // mover method: //works only with -1 or 1
  void step(int number_of_steps);
};

#endif 
