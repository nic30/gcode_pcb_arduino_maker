#include "motorManager.h"

/**
 * Set the feedrate (speed motors will move)
 * @input nfr the new speed in steps/second
 */
void MotorManager::feedrate(float nfr) {
  if(fr==nfr) return;  // same as last time?  quit now.

  if(nfr>MAX_FEEDRATE || nfr<MIN_FEEDRATE) {  // don't allow crazy feed rates
    Serial.print(F("New feedrate must be greater than "));
    Serial.print(MIN_FEEDRATE);
    Serial.print(F("steps/s and less than "));
    Serial.print(MAX_FEEDRATE);
    Serial.println(F("steps/s."));
    return;
  }
  step_delay = 1000000.0/nfr;
  fr=nfr;
}


void  MotorManager::release() {
  m1.release();
  m2.release();
}


/**
 * Supports movement with both styles of Motor Shield
 * @input newx the destination x position
 * @input newy the destination y position
 **/
void MotorManager::onestep(int motor,int direction) {
  if(direction >0){
    direction = 1;
  }
  else{
    direction = -1;
  }
  if(motor==1) {
#ifdef VERBOSE
    Serial.print('X');
#endif
    m1.step(direction*AXIS_X_DIR);
  } 
  else {
#ifdef VERBOSE
    Serial.print('Y');
#endif
    m2.step(direction*AXIS_Y_DIR);
  }
}




