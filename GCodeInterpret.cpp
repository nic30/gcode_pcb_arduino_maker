#include "GCodeInterpret.h"


GCodeInterpret:: GCodeInterpret(char * buffer, int * sofarAddr){
  this->buffer = buffer;
  this->sofarAddr = sofarAddr;
  pinMode(AXIS_X_MIN_PIN, INPUT);
  pinMode(AXIS_Y_MIN_PIN, INPUT);
}

void GCodeInterpret::init(){
  help();  // say hello
  position(0,0);  // set staring position
  mm.feedrate(300);  // set default speed
  ready();
}

/**
 * delay for the appropriate number of microseconds
 * @input ms how many milliseconds to wait
 */
void GCodeInterpret::pause(long ms) {
  delay(ms/1000);
  delayMicroseconds(ms%1000);  // delayMicroseconds doesn't work for values > ~16k.
}

void GCodeInterpret::setLaser(int pwm){
  if(pwm <0){
    pwm =0;
  }
  else{
    if(pwm >255){
      pwm = 255;
    }
  }
  analogWrite(LASER_PIN, pwm);
}

/**
 * Set the logical position
 * @input npx new position x
 * @input npy new position y
 */
void GCodeInterpret::position(float npx,float npy) {
  // here is a good place to add sanity tests
  px=npx;
  py=npy;
}

/**
 * Uses bresenham's line algorithm to move both motors
 * @input newx the destination x position
 * @input newy the destination y position
 **/
void GCodeInterpret::line(float newx,float newy) {
  long dx=newx-px;
  long dy=newy-py;
  int dirx=dx>0?1:-1;
  int diry=dy>0?-1:1;  // because the motors are mounted in opposite directions
  dx=abs(dx);
  dy=abs(dy);

  long i;
  long over=0;

#ifdef VERBOSE
  Serial.println(F("Start >"));
#endif

  if(dx>dy) {
    for(i=0;i<dx;++i) {
      mm.onestep(1,dirx);
      over+=dy;
      if(over>=dx) {
        over-=dx;
        mm.onestep(2,diry);
      }
      pause(mm.step_delay);
    }
  } 
  else {
    for(i=0;i<dy;++i) {
      mm.onestep(2,diry);
      over+=dx;
      if(over>=dy) {
        over-=dy;
        mm.onestep(1,dirx);
      }
      pause(mm.step_delay);
    }
  }

#ifdef VERBOSE
  Serial.println(F("< Done."));
#endif

  px=newx;
  py=newy;
}


/**
 * Look for character /code/ in the buffer and read the float that immediately follows it.
 * @return the value found.  If nothing is found, /val/ is returned.
 * @input code the character to look for.
 * @input val the return value if /code/ is not found.
 **/
float GCodeInterpret::parsenumber(char code,float val) {
  char *ptr=buffer;
  while(ptr && *ptr && ptr<buffer+*sofarAddr) {
    if(*ptr==code) {
      return atof(ptr+1);
    }
    ptr=strchr(ptr,' ')+1;
  }
  return val;
} 

/**
 * write a string followed by a float to the serial line.  Convenient for debugging.
 * @input code the string.
 * @input val the float.
 */
void GCodeInterpret::output(char *code,float val) {
  Serial.print(code);
  Serial.println(val);
}

/**
 * print the current position, feedrate, and absolute mode.
 */
void GCodeInterpret::where() {
  output("X",px);
  output("Y",py);
  output("F",mm.fr);
  Serial.println(mode_abs?"ABS":"REL");
}

/**
 * display helpful information
 */
void GCodeInterpret::help() {
  Serial.print(F("GcodeCNCDemo2AxisV2 "));
  Serial.println(VERSION);
  Serial.println(F("Commands:"));
  Serial.println(F("G00 [X(steps)] [Y(steps)] [F(feedrate)]; - linear move"));
  Serial.println(F("G01 [X(steps)] [Y(steps)] [F(feedrate)]; - linear move"));
  Serial.println(F("G04 P[ms]; - delay"));
  Serial.println(F("G28 return home and calibrate position to 0 0"));
  Serial.println(F("G90; - absolute mode"));
  Serial.println(F("G91; - relative mode"));
  Serial.println(F("G92 [X(steps)] [Y(steps)]; - change logical position"));
  Serial.println(F("T0 [P(pwm 0 -255)]; - change pwm of laser (0 = disabled)"));
  Serial.println(F("M18; - disable motors"));
  Serial.println(F("M100; - this help message"));
  Serial.println(F("M114; - report position and feedrate"));
}


/**
 * Read the input buffer and find any recognized commands.  One G or M command per line.
 */
void GCodeInterpret::processCommand() {
  int cmd = parsenumber('G',-1);
  switch(cmd) {
  case  0: // move linear
  case  1: // move linear
    mm.feedrate(parsenumber('F',mm.fr));
    line( 
    parsenumber('X',(mode_abs?px:0)) + (mode_abs?0:px), 
    parsenumber('Y',(mode_abs?py:0)) + (mode_abs?0:py) 
      );
    break;
  case  4:  
    delay(parsenumber('P',0));  
    break;  // dwell
  case 28:
    returnHome();
    break;
  case 90:  
    mode_abs=1;  
    break;  // absolute mode
  case 91:  
    mode_abs=0;  
    break;  // relative mode
  case 92:  // set logical position
    position( parsenumber('X',0),
    parsenumber('Y',0) );
    break;
  default:  
    break;
  }

  cmd = parsenumber('M',-1);
  switch(cmd) {
  case 18:  // disable motors
    mm.release();
    break;
  case 100:  
    help();  
    break;
  case 114: 
    where();  
    break;
  default:  
    break;
  }

  cmd = parsenumber('T',-1);
  switch(cmd) {
  case 0:
    setLaser(parsenumber('P', 0.0));
    break;
  default:
    break;
  }
 //Serial.print("Sofar is:");
 //Serial.println(*sofarAddr);
 
}

/**
 * prepares the input buffer to receive a new message and tells the serial connected device it is ready for more.
 */
void GCodeInterpret::ready() {
  *sofarAddr=0;  // clear input buffer
  Serial.print(F(">OK\n"));  // signal ready to receive input
}

void GCodeInterpret::returnHome(){
  boolean isX_Min = false;
  boolean isY_Min = false;
  while(!(isX_Min && isY_Min)){
    isX_Min = digitalRead(AXIS_X_MIN_PIN);
    if(! isX_Min){
      line(px-1, py);
    }
    isY_Min = digitalRead(AXIS_Y_MIN_PIN);
    if(! isY_Min){
      line(px, py-1);
    }
  }
  px = 0;
  py = 0;
}







