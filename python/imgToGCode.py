from PIL import Image
from __builtin__ import xrange
from gcodeCreator import GcodeCreator
import os
import copy

filename = "scheme.png"
size = 1.524, 1.778 #size in cm

class Point():
    x = 0
    y = 0
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)
    
    def __repr__(self):
        return "Point( %d , %d )" % (self.x, self.y)

class GCodeConvertor():
    feedrate = 300
    pixelRatio = (116.667, 111.111)
    threndshod = (100,100,100)
    invert = False  
    laserLow = 0
    laserHigh = 255
    gc = GCodeCreator()
    img = None
    
    def __init__(self, filename, size):
        self.filename = filename
        self.size = size
        self.lastPoint = Point(0, 0)
        self.lastPoint.positive = False
        
    def colorPoint(self, p):
        pixel = self.img.getpixel((p.x, p.y))
        if self.invert:
            p.positive = pixel[0] > self.threndshod[0] or pixel[1] > self.threndshod[1] or pixel[2] > self.threndshod[2]
        else:
            p.positive = pixel[0] < self.threndshod[0] or pixel[1] < self.threndshod[1] or pixel[2] < self.threndshod[2]
        return p
        
    def processPoint(self, p):
        p = self.colorPoint(p)
        # row up
        if self.lastPoint.positive and p.x > self.lastPoint.x:
                self.gc.position(p.x, p.y, self.feedrate)
                self.gc.release()
        
        #start
        if p.positive and not self.lastPoint.positive:
                self.gc.position(p.x, p.y, self.feedrate)
                self.gc.turnLaser(self.laserHigh)
                self.gc.release()
        #end
        if not p.positive and self.lastPoint.positive:
            self.gc.position(p.x, p.y, self.feedrate)
            self.gc.turnLaser(self.laserLow)
            self.gc.release()
            
            
        self.lastPoint = copy.deepcopy(p)
    
    def logProgress(self,p):
        if p.x % (self.size[0] / 100) == 0:
            print(str((p.x / float(self.size[0])) * 100) + "%") 
    
    def resizeImg(self):
        self.size = (int(self.size[0] * self.pixelRatio[0]) , int( self.size[1] * self.pixelRatio[1]))
        self.img = Image.open(self.filename)
        self.img.thumbnail(self.size, Image.ANTIALIAS)
        self.img.save("out.bmp")
        self.img = Image.open("out.bmp")
        self.size = self.img.size
        
    def convert(self):
        self.resizeImg()
        p = Point(0,0)
        
        self.gc.absolute()
        self.gc.returnHome()
        while p.x < self.size[0]:
            while p.y < self.size[1]:
                self.processPoint(p)
                p.y += 1
            p.x += 1
            if p.x >= self.size[0]:
                break
            p.y = self.size[1] - 1
            self.processPoint(p)
            while p.y > 0:
                self.processPoint(p)
                p.y -= 1
            p.x += 1
            p.y = 0
            self.logProgress(p)
            if p.x < self.size[0]:
                self.processPoint(p)

        self.gc.turnLaser(0)
        self.gc.release()
        self.gc.returnHome()
        self.gc.release()        
        self.gc.flushToFile()
        os.remove("out.bmp")

if __name__ == "__main__":
    gco = GCodeConvertor(filename, size)
    gco.convert()