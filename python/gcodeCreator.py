from random import randrange
from cStringIO import StringIO

class GCodeCreator:
    buffer = StringIO()
    
    def returnHome(self):
        self.buffer.write("G28;\n")

    def release(self):
       self.buffer.write("M18;\n")
    
    def absolute(self):
       self.buffer.write( "G90;\n")
    
    def position(self,x, y, feedrate):
       self.buffer.write("G00 X%d Y%d F%d;\n" % (x,y,feedrate))
    
    def turnLaser(self,pwm):
       self.buffer.write( "T0 P%d;\n" % pwm)

    def delay(self, seconds):
       self.buffer.write("G04 P%d;\n" % seconds)
    
    def flushToFile(self):
        with open("gcode.txt", "w") as f:
           f.write(self.buffer.getvalue())

