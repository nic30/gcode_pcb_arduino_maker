import serial, sys
port_id = "/dev/ttyUSB0"
baud = 115200
filename = "gcode.txt"


def flusth(): 
    print "FLUSHING..."
    ser = serial.Serial(port=port_id, baudrate=baud, timeout=2)
    while True:
        response = ''
        response = ser.readline()
        print response.strip('\n')
        if response is '':
            break
    ser.close()

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def logProgress(l):
    if l % (fLen/100) ==0:
        print(str( (l / float(fLen)) * 100) + "%") 

flusth()
 
ser = serial.Serial(port=port_id, baudrate=baud, timeout=3)
fLen = file_len(filename)
i =0
with open(filename) as f:
    for line in f:
        logProgress(i)
        line = ' '.join(line.split())
        ser.write(line)
        #print "send: %s" % line
        while True:
           incomming = ser.readline().split()
           #print "incoming: %s" % incomming
           if ">OK" in incomming[-1]:
              break
        i+=1
ser.close()
