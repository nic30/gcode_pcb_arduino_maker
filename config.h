
#ifndef CONFIG_H
#define CONFIG_H

#define LASER_PIN 3

// type of steppers
//#define I2C_STEPPER
#define DIRECT_STEPPER // use stepper.h from arduino


#ifdef I2C_STEPPER
  #ifdef DIRECT_STEPPER
    #error I2C_STEPPER and DIRECT_STEPPER canot be used together
  #endif
  #define X_ADDR 126
  #define Y_ADDR 125
  #define Z_ADDR 124

#endif
  #define CMD_STEP_FOWARD 'f'
  #define CMD_STEP_BACKWARD 'b'
  #define CMD_RELEASE 'r'

#define AXIS_X_DIR -1
#define AXIS_Y_DIR 1

#ifdef DIRECT_STEPPER
  #ifdef I2C_STEPPER
    #error I2C_STEPPER and DIRECT_STEPPER canot be used together
  #endif

  #define M2_PIN1 4
  #define M2_PIN2 5
  #define M2_PIN3 6
  #define M2_PIN4 7
  
  #define M1_PIN1 8
  #define M1_PIN2 9
  #define M1_PIN3 10
  #define M1_PIN4 11

#endif

#define AXIS_X_MIN_PIN A0
#define AXIS_Y_MIN_PIN A1

//#define VERBOSE              (1)  // add to get a lot more serial output.

#define VERSION              (2)  // firmware version
#define BAUDRATE             (115200)  // Serial baudrate
#define MAX_BUF              (64)  // What is the longest message Arduino can store?
#define STEPS_PER_TURN       (200)  // depends on your stepper motor.  most are 200.
#define MIN_STEP_DELAY       (20)
#define MAX_FEEDRATE         (1000000/MIN_STEP_DELAY)
#define MIN_FEEDRATE         (0.01)






#endif 


